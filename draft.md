unfinished more-complete power-routing how-to / explainer

# Power Distribution Boards

While [squidworks](link) describes how we might ideally ferry data around a machine, it makes no mention of control's second, and arguably more fun / dangerous / exciting ingredient: power. Getting the right amounts, and right kinds of power to any mechatronic system is an important step, and there are many ways to do it.

For our purposes, it's convenient to think of power as a graph. Rather, as some set of overlaid graphs with potentials between them. In the following, I will assume systems being discussed are purely DC and will rarely exceed 1.5kW total consumption.

`! achtung !`

*high voltages (like the 110v (north america) and 220v (europe) in many wall sockets) can be dangerous, take care, and make sure you understand what you're doing before you do it, especially where wall voltages (or other high voltages) are involved*

*if care is not taken during power routing, it is easy to fry / explode / burn / otherwise disable sensitive control circuitry: don't do this: be careful, check things twice, if you don't have a good idea about what will happen when you flip a switch, refrain from flipping that switch until you do*

`! achtung !`

## The Ground Net

We should start with the ground network. To avoid [ground loops](https://en.wikipedia.org/wiki/Ground_loop_(electricity)), a devices sharing a common ground should always do so with a [tree](https://en.wikipedia.org/wiki/Tree_(graph_theory)) - the root of this tree will typically be some (set) of power supplies, or - as is often the case in automobile wiring - the [chassis](https://en.wikipedia.org/wiki/Chassis_ground) of the machine itself. This constitutes the kind of power backbone of our system: everything is connected to ground, all the time.

![gnd-start](ground-net)

## The Voltage Nets

Ground is great, but we need some potential (voltage) in order to do anything useful. While all devices on our power network should have one connection to ground, they will likely otherwise have different voltage desires. This commonly means `5v` or `3v3` for logic level devices, or `12v`, `24v`, or `48v` for high power devices (motors, lights, heaters, etc). Many high power devices will have a logic voltage input as well as a power voltage input: this is the case with [these stepper drivers]() that require 5v of logic and expect between 12v and 35v of motor power voltage.

... for example: (some system with n motors (logic and power), n sensors (logic only)) ... setup for multiple 5v drops

... voltages are also trees, their roots being whatever-device generated that voltage  

... it's typical that lower voltages are logic for higher voltages, so care should be taken to power on smaller voltages before bigger ones, lest we blow anything up.

... other voltages can be split

... recall that a traditional north american circuit breaker trips at 17 amps, while being rated (i.e. suggested use) of 15amps. At 110v, this is ~ 1600 Watts.

## Help

Things can go wrong with power electronics. To help keep things in order, we use some small helpers:

... bypass capacitors
 ... bleed resistors

... tvs diodes

## More!

Continued notes, i.e:
 - wireless links? (ok: two graphs)
 - long links (earth reference?)
 - battery operated (no problems, just battery instead of psu)
 - two psus (ok, more power more money)

... 4-ax machine

ok, we can combine psus to pull more total power: they're not really working in parallel here, but they are working together:

... 4-ax w/ 1kw spindle (2x psu)

and we can put different voltages together, so long as we keep the common ground together:

... sys w/ high voltage transformer to ultrasonic driver, or similar
... (psus should probably be on the same circuit: actually not sure about upstream AC...)

psus can be batteries:

... some battery powered system
