# Power Distribution Boards

While [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) describes how we might ideally ferry data around a machine, it makes no mention of control's second, and arguably more fun / dangerous / exciting ingredient: power. Getting the right amounts, and right kinds of power to any mechatronic system is an important step, and there are many ways to do it.

`! achtung !`

>*high voltages (like the 110v (north america) and 220v (europe) in many wall sockets) can be dangerous. take care, and make sure you understand what you're doing before you do it, especially where wall voltages (or other high voltages) are involved*

>*if care is not taken during power routing, it is easy to fry / explode / burn / otherwise disable sensitive control circuitry: don't do this: be careful, check things twice, if you don't have a good idea about what will happen when you flip a switch, refrain from flipping that switch until you do*

`! achtung !`

## Nets

For our purposes, it's nice to think of power as a handful of overlaid graphs: in EE, it's common to call these 'nets'. For most machines, we use three nets: one common Ground, one 5v net for logic power, and one 24v net for actuator power.

### The Ground Net

To avoid [ground loops](https://en.wikipedia.org/wiki/Ground_loop_(electricity)), a devices sharing a common ground should always do so with a [tree](https://en.wikipedia.org/wiki/Tree_(graph_theory)) - the root of this tree will typically be some (set) of power supplies, or - as is often the case in automobile wiring - the [chassis](https://en.wikipedia.org/wiki/Chassis_ground) of the machine itself. This constitutes the kind of power backbone of our system: everything is connected to the same ground, all the time, no matter what voltages it consumes.

It's important to recall that when we connect a laptop to a machine over USB, we should hope that the laptop's ground reference is somewhere near the machine's. This *may or not be a problem* to worry about, so it's best to just handle it. To do so, make sure that any laptop that is connected to a machine component is also plugged in and charging, with the laptop's charger on the same AC circuit as the machine's power supply.

### The Logic Voltage Net

Once things are hooked up to ground, we can deliver logic-level voltage to our devices. Processors typically run very low voltages (say, 3v3 in modern microcontrollers, 1v7 in even *fancier* systems, or 5v in older micros), and I often choose to bus one, connected, 5v line around a machine. This way, I can turn all of the communicating devices on or off simultaneously, which makes things simpler. 5v is a great candidate because I can use a smaller 3v3 LDO regulator at each chip (adding some local stability), and because some devices (sensors) want a 5v line. Including 5v in these systems also makes RC Servos easy to integrate.

Also! 5v is available from a USB connection, making it that much handier.

### The Power Voltage Nets

It's rare to use voltages as low as 5v in any appreciable mechatronic system. This is because higher voltages allow us to transmit more power while accumulating smaller losses due to resistance. This is the same reason that we bus power around 'the grid' in the kilovolts, and transform it down to lower voltages at endpoints. Additionally, since we are often switching large inductive loads (motors) at the endpoints, bigger voltages give us bigger control authorities.

For most purposes, I am satisfied with a 24v bus. This means that almost all power electronics parts available on the market can be used (35v is a common cap), the voltage is not wildly dangerous, and power supplies in this range are also widely available.

## In Practice

To help route power, I have this series of circuits that I call 'pdbs' - these are fairly new to me, but so far they are more helpful than not. They let me bus voltage around machines in a fairly ad-hoc, but still organized manner.

First, I can use them to assemble a set of the basic components I want at the head of each Switching Power Supply I'll use:

![psu-img](images/2019-11-19_pdb-explainer-1.png)

This includes:   

`5V Regulator Module` - this pulls some power from the 24V net and regulates it down to 5V, for the logic net. Each of these modules can supply 2A of 5V, which is typically enough for one 'desktop' sized machine's logic demand. Additional 5V power (i.e. for servos) should be provided with a separate 5V module, or an additional 5V switching power supply.

`TVS Diode and Bleeder Resistor Module` - a TVS (Transient Voltage Suppression) diode 'clamps' voltage spikes that might show up on the 24V net. The bleeder resistor is here to drain the big capacitor safely when the power is switched off.

`Big Capacitor Module` - this is just a lot of spare capacitance. It helps to smooth instantaneous power demands that stepper motors or other power electronics are likely to make, preventing power electronic brownout.

![psu-img](images/2019-11-19_pdb-explainer-2.png)

## Kit

Circuit drawings are included in this repo, for each of the components mentioned.

### psu-mw350

[This includes the CAD file for the PSU mount pictured.](/psu-mw350)

![psuimg](images/2019-11-19_pdb-2.jpg)
![psuimg](images/2019-11-19_pdb-3.jpg)

### pdb-5vreg

Peel 5v from 24v!

![pdbreg](pdb-5vreg/routed.png)

### pdb-3

Branch!

![pd3](pdb-3/routed.png)

### pdb-bfc

Big Capacitance!

![pdbfc](pdb-bfc/routed.png)

### pdb-bleeder

Bleed off big capacitance, include TVS diode to eat nasty spikes.

![pdbleed](pdb-bleeder/routed.png)
